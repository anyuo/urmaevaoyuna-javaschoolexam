package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        listValidation(x,y);
        if(checkEmptyX(x))
            return true;
        return checkSubsequence(x,y);
    }
    private void listValidation(List x, List y){
        if (x == null || y == null)
            throw new IllegalArgumentException();
    }
    private boolean checkEmptyX(List x){
        return x.size() == 0;
    }
    private boolean checkSubsequence(List x, List y){
        int numberX = 0;
        int numberY = 0;
        while (numberX < x.size()){
            boolean foundPair = false;
            while (numberY < y.size()){
                if (x.get(numberX) == y.get(numberY)) {
                    foundPair = true;
                    break;
                } else {
                    numberY++;
                }
            }
            if(!foundPair)
                return false;
            numberX++;
        }
        return true;
    }
}
