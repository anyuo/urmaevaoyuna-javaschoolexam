package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    private int pyramidHeight;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        listValidation(inputNumbers);
        Collections.sort(inputNumbers);
        int[][] pyramidTable = new int [pyramidHeight][pyramidHeight*2-1];
        return pyramidFill(inputNumbers,pyramidTable);
    }
    public PyramidBuilder(){
        pyramidHeight = 0;
    }
    private long dimensionCounter(List<Integer> inputNumbers){
        long numbersForBuild = 0;
        while (numbersForBuild < inputNumbers.size()){
            pyramidHeight++;
            numbersForBuild += pyramidHeight;
        }
        return numbersForBuild;
    }
    private void listValidation(List<Integer> inputNumbers){
        if (dimensionCounter(inputNumbers) != inputNumbers.size()) {
            throw new CannotBuildPyramidException();
        }
        for (int i = 0; i < inputNumbers.size(); i++) {
            if(inputNumbers.get(i) == null)
                throw new CannotBuildPyramidException();
        }
    }
    private int[][] pyramidFill(List<Integer> inputNumbers, int[][] pyramidTable){
        int firstPosition = pyramidTable.length - 1;
        int lastPosition = pyramidTable.length - 1;
        int numberInList = 0;
        for (int i = 0; i < pyramidTable.length; i++) {
            int unitPosition = firstPosition;
            while(unitPosition <= lastPosition){
                pyramidTable[i][unitPosition] = inputNumbers.get(numberInList);
                unitPosition +=2;
                numberInList++;
            }
            firstPosition--;
            lastPosition++;
        }
        return pyramidTable;
    }


}
