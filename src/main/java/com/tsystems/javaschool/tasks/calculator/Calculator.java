package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;

public class Calculator {

    private final String ALLOWABLE_CHARACTERS = "1234567890()+-*/.";
    private boolean divByZero;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if(invalidStatement(statement))
            return null;
        double result = arithmetics(statement);
        if (divByZero)
            return null;
        if (result % 1 == 0)
            return String.format("%.0f", result);
        return Double.toString(result);
    }
    public Calculator(){
        divByZero = false;
    }
    private boolean invalidStatement(String statement){
        if(statement == null || statement.length() == 0)
            return true;
        int bracketsCounter = 0;
        for (int i = 0; i < statement.length(); i++) {
            char currentSymbol = statement.charAt(i);
            if(!ALLOWABLE_CHARACTERS.contains(Character.toString(currentSymbol))){
                return true;
            }
            if (currentSymbol == '(')
                bracketsCounter++;
            if (currentSymbol == ')')
                bracketsCounter--;
            if (bracketsCounter < 0)
                return true;
            if(isOperator(currentSymbol))
                if(!(statement.charAt(i+1) == '(' || Character.isDigit(statement.charAt(i+1))))
                    return true;
            if(currentSymbol == '.')
                if(!(Character.isDigit(statement.charAt(i-1)) && Character.isDigit(statement.charAt(i+1))))
                    return true;
        }
        if (bracketsCounter != 0)
            return true;
        return false;
    }
    private boolean isOperator(char operator) {
        return (operator == '+' || operator == '-' || operator == '*' || operator == '/');
    }
    private int priority(char operator) {
        switch (operator) {
            case '+':
                return 1;
            case '-':
                return 1;
            case '*':
                return 2;
            case '/':
                return 2;
            default:
                return -1;
        }
    }
    private void executeOperation(LinkedList<Double> digitsList, char operator) {
        double r = digitsList.removeLast();
        double l = digitsList.removeLast();
        switch (operator) {
            case '+':
                digitsList.add(l + r);
                break;
            case '-':
                digitsList.add(l - r);
                break;
            case '*':
                digitsList.add(l * r);
                break;
            case '/':
                if (r != 0)
                    digitsList.add(l / r);
                else
                    divByZero = true;
                break;
        }
    }
    private double arithmetics(String statement) {
        LinkedList<Double> digitsList = new LinkedList<Double>();
        LinkedList<Character> operatorsList = new LinkedList<Character>();
        for (int i = 0; i < statement.length(); i++) {
            char currentSymbol = statement.charAt(i);
            if (currentSymbol == '(') {
                operatorsList.add('(');
            } else if (currentSymbol == ')') {
                while (operatorsList.getLast() != '(')
                    executeOperation(digitsList,operatorsList.removeLast());
                operatorsList.removeLast();
            } else if (isOperator(currentSymbol)) {
                while (!operatorsList.isEmpty() && priority(operatorsList.getLast()) >= priority(currentSymbol))
                    executeOperation(digitsList, operatorsList.removeLast());
                operatorsList.add(currentSymbol);
            } else {
                String digit = "";
                while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.'))
                    digit += statement.charAt(i++);
                --i;
                digitsList.add(Double.parseDouble(digit));
            }
            if(divByZero)
                break;
        }
        while (!operatorsList.isEmpty())
            executeOperation(digitsList, operatorsList.removeLast());
        if (divByZero)
            return 0;
        else
            return digitsList.get(0);
    }

}
